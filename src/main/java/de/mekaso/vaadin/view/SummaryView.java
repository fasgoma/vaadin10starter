package de.mekaso.vaadin.view;

import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterObserver;
import com.vaadin.flow.router.Route;

import de.mekaso.vaadin.layout.ApplicationLayout;

@Route(value = "summary", layout = ApplicationLayout.class)
public class SummaryView extends FormLayout implements BeforeEnterObserver {
	private static final long serialVersionUID = 1L;

	private Label label;
	public SummaryView() {
		this.label = new Label();
		super.add(label);
	}
	@Override
	public void beforeEnter(BeforeEnterEvent event) {
		this.label.setText("SummaryView");
		
	}
}
