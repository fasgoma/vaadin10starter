package de.mekaso.vaadin.view;

import javax.annotation.PostConstruct;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcons;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.Notification.Position;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.i18n.I18NProvider;
import com.vaadin.flow.i18n.LocaleChangeEvent;
import com.vaadin.flow.i18n.LocaleChangeObserver;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterObserver;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;

import de.mekaso.vaadin.layout.ApplicationLayout;

@PageTitle("Vaadin10 Starter Project")
@Route(value = "", layout = ApplicationLayout.class)
public class PersonView extends FormLayout implements BeforeEnterObserver, LocaleChangeObserver {
	private static final long serialVersionUID = 1L;
	
	@Inject
	private I18NProvider i18NProvider;
	
	private TextField nameField;
	private TextField firstnameField;
	private DatePicker birthday;
	private Button validationButton;
	
	@PostConstruct
	protected void init() {
		this.nameField = new TextField();
		this.firstnameField = new TextField();
        this.validationButton = new Button(new Icon(VaadinIcons.AMBULANCE));
        this.birthday = new DatePicker();
        addBehaviour();
	}
	
	private void addBehaviour() {
		this.validationButton.addClickListener(event -> Notification.show("Click.", 3000, Position.MIDDLE));
		this.validationButton.setAutofocus(true);
	}

	@Override
	public void beforeEnter(BeforeEnterEvent event) {
		add(this.nameField, this.firstnameField, this.birthday, this.validationButton);
	}

	@Override
	public void localeChange(@Observes LocaleChangeEvent event) {
		this.nameField.setLabel(this.i18NProvider.getTranslation("person.name", event.getLocale()));
		this.firstnameField.setLabel(this.i18NProvider.getTranslation("person.firstname", event.getLocale()));
		this.validationButton.setText(this.i18NProvider.getTranslation("person.button", event.getLocale()));
		this.birthday.setLabel(this.i18NProvider.getTranslation("person.birthday", event.getLocale()));
		this.birthday.setLocale(event.getLocale());
	}
}